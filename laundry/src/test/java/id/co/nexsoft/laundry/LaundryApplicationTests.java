package id.co.nexsoft.laundry;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.boot.test.context.SpringBootTest;

import id.co.nexsoft.laundry.controller.ServiceController;
import id.co.nexsoft.laundry.model.Services;

@SpringBootTest
@ExtendWith(MockitoExtension.class)
class LaundryApplicationTests {

	// @Mock
    // private DefaultService defaultService;

    @InjectMocks
    private ServiceController serviceController;

    @Test
    public void testGetDataEndpoint() throws Exception {
		List<Services> serviceList = Arrays.asList(
			new Services("service 1", 17000.0, "Weight", "Regular"),
			new Services("service 2", 18000.0, "Weight", "Regular"));

		when(serviceController.getAllData()).thenReturn(serviceList);
		List<Services> result = serviceController.getAllData();

		assertEquals(serviceList.size(), result.size());
    }

}
