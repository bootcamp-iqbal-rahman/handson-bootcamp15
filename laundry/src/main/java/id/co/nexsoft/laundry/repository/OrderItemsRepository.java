package id.co.nexsoft.laundry.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.laundry.model.OrderItems;

public interface OrderItemsRepository extends CrudRepository<OrderItems, Integer> {
    List<OrderItems> findAll();
    Optional<OrderItems> findById(int id);
}
