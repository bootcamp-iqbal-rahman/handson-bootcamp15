package id.co.nexsoft.laundry.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.laundry.model.Order;

public interface OrderRepository extends CrudRepository<Order, Integer> {
    List<Order> findAll();
    Optional<Order> findById(int id);
}
