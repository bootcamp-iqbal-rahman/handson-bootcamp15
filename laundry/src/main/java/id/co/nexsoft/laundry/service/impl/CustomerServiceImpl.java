package id.co.nexsoft.laundry.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.laundry.model.Customer;
import id.co.nexsoft.laundry.repository.CustomerRepository;
import id.co.nexsoft.laundry.service.DefaultService;

@Service
public class CustomerServiceImpl implements DefaultService<Customer> {
    @Autowired
    private CustomerRepository customerRepository;
    
    @Override
    public List<Customer> getAllData() {
        return customerRepository.findAll();
    }

    @Override
    public Customer getDataById(int id) {
        return customerRepository.findById(id).get();
    }

    @Override
    public Customer saveData(Customer data) {
        return customerRepository.save(data);
    }

    @Override
    public ResponseEntity<Customer> updateData(Customer data, int id) {
        Optional<Customer> customer = customerRepository.findById(id);

        if (!customer.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        data.setId(id);
        customerRepository.save(data);
        return ResponseEntity.ok(data);
    }

    @Override
    public void deleteData(int id) {
        Optional<Customer> customerList = customerRepository.findById(id);
        Customer customer = customerList.get();

        customer.setDeletedDate(LocalDate.now());
        customerRepository.save(customer);
    }
}