package id.co.nexsoft.laundry.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.laundry.model.OrderItems;
import id.co.nexsoft.laundry.repository.OrderItemsRepository;
import id.co.nexsoft.laundry.service.DefaultService;

@Service
public class OrderItemsServiceImpl implements DefaultService<OrderItems> {
    @Autowired
    private OrderItemsRepository serviceRepository;
    
    @Override
    public List<OrderItems> getAllData() {
        return serviceRepository.findAll();
    }

    @Override
    public OrderItems getDataById(int id) {
        return serviceRepository.findById(id).get();
    }

    @Override
    public OrderItems saveData(OrderItems data) {
        return serviceRepository.save(data);
    }

    @Override
    public ResponseEntity<OrderItems> updateData(OrderItems data, int id) {
        Optional<OrderItems> orderItems = serviceRepository.findById(id);

        if (!orderItems.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        data.setId(id);
        serviceRepository.save(data);
        return ResponseEntity.ok(data);
    }

    @Override
    public void deleteData(int id) {
    }
}