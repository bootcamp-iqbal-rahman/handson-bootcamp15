package id.co.nexsoft.laundry.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "address")
public class Address {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    
    @ManyToOne
    @JoinColumn(name = "customer_id", referencedColumnName = "id")
    @JsonBackReference(value = "customer-address")
    private Customer customer_id;

    @ManyToOne
    @JoinColumn(name = "employee_id", referencedColumnName = "id")
    @JsonBackReference(value = "employee-address")
    private Employee employee_id;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "street")
    private String street;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "district")
    private String district;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "city")
    private String city;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "province")
    private String province;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "country")
    private String country;

    @Column(name = "deleted_date")
    private LocalDate deletedDate;

    public Address() {}

    public Address(Customer customer_id, Employee employee_id, String street, String district, 
                    String city, String province, String country) {
        this.customer_id = customer_id;
        this.employee_id = employee_id;
        this.street = street;
        this.district = district;
        this.city = city;
        this.province = province;
        this.country = country;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Customer getCustomer_id() {
        return customer_id;
    }

    public void setCustomer_id(Customer customer_id) {
        this.customer_id = customer_id;
    }

    public Employee getEmployee_id() {
        return employee_id;
    }

    public void setEmployee_id(Employee employee_id) {
        this.employee_id = employee_id;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public LocalDate getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDate deletedDate) {
        this.deletedDate = deletedDate;
    }
}