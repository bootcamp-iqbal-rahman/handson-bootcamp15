package id.co.nexsoft.laundry.service;

import java.util.List;

import org.springframework.http.ResponseEntity;

public interface DefaultService<T> {
    List<T> getAllData();
    T getDataById(int id);
    T saveData(T data);
    ResponseEntity<T> updateData(T data, int id);
    void deleteData(int id);
}
