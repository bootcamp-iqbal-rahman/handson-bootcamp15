package id.co.nexsoft.laundry.model;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

@Entity
@Table(name = "employee")
public class Employee {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "name")
    private String name;

    @NotNull(message = "Field cannot be null")
    @Column(name = "birth_date")
    private LocalDate birthData;

    @OneToMany(mappedBy = "employee_id", cascade = CascadeType.ALL)
    @JsonManagedReference(value = "employee-address")
    private List<Address> address_list;

    @Pattern(regexp = "^[0-9]+$", message = "Phone number should only contain numbers")
    @Column(name = "phone")
    private long phone;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Email(message = "Email not valid")
    @Column(name = "email")
    private String email;

    @Column(name = "deleted_date")
    private LocalDate deletedDate;

    public Employee() {}

    public Employee(String name, LocalDate birthData, long phone, String email) {
        this.name = name;
        this.birthData = birthData;
        this.phone = phone;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalDate getBirthData() {
        return birthData;
    }

    public void setBirthData(LocalDate birthData) {
        this.birthData = birthData;
    }

    public List<Address> getAddress_list() {
        return address_list;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDate deletedDate) {
        this.deletedDate = deletedDate;
    }
}
