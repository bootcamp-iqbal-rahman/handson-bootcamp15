package id.co.nexsoft.laundry.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.laundry.model.Address;
import id.co.nexsoft.laundry.repository.AddressRepository;
import id.co.nexsoft.laundry.service.DefaultService;

@Service
public class AddressServiceImpl implements DefaultService<Address> {
    @Autowired
    private AddressRepository addressRepository;
    
    @Override
    public List<Address> getAllData() {
        return addressRepository.findAll();
    }

    @Override
    public Address getDataById(int id) {
        return addressRepository.findById(id).get();
    }

    @Override
    public Address saveData(Address data) {
        return addressRepository.save(data);
    }

    @Override
    public ResponseEntity<Address> updateData(Address data, int id) {
        Optional<Address> address = addressRepository.findById(id);

        if (!address.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        data.setId(id);
        addressRepository.save(data);
        return ResponseEntity.ok(data);
    }

    @Override
    public void deleteData(int id) {
        Optional<Address> addressList = addressRepository.findById(id);
        Address address = addressList.get();

        address.setDeletedDate(LocalDate.now());
        addressRepository.save(address);
    }
}