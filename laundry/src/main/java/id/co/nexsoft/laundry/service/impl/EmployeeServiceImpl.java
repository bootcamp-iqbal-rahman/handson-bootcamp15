package id.co.nexsoft.laundry.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.laundry.model.Employee;
import id.co.nexsoft.laundry.repository.EmployeeRepository;
import id.co.nexsoft.laundry.service.DefaultService;

@Service
public class EmployeeServiceImpl implements DefaultService<Employee> {
    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Override
    public List<Employee> getAllData() {
        return employeeRepository.findAll();
    }

    @Override
    public Employee getDataById(int id) {
        return employeeRepository.findById(id).get();
    }

    @Override
    public Employee saveData(Employee data) {
        return employeeRepository.save(data);
    }

    @Override
    public ResponseEntity<Employee> updateData(Employee data, int id) {
        Optional<Employee> employee = employeeRepository.findById(id);

        if (!employee.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        data.setId(id);
        employeeRepository.save(data);
        return ResponseEntity.ok(data);
    }

    @Override
    public void deleteData(int id) {
        Optional<Employee> employeeList = employeeRepository.findById(id);
        Employee employee = employeeList.get();

        employee.setDeletedDate(LocalDate.now());
        employeeRepository.save(employee);
    }
}