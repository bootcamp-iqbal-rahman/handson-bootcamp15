package id.co.nexsoft.laundry.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.laundry.model.Employee;
import id.co.nexsoft.laundry.model.Order;
import id.co.nexsoft.laundry.repository.EmployeeRepository;
import id.co.nexsoft.laundry.repository.OrderRepository;
import id.co.nexsoft.laundry.service.DefaultService;
import id.co.nexsoft.laundry.service.UpdateService;

@Service
public class OrderServiceImpl implements DefaultService<Order>, UpdateService<Order> {
    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private EmployeeRepository employeeRepository;
    
    @Override
    public List<Order> getAllData() {
        return orderRepository.findAll();
    }

    @Override
    public Order getDataById(int id) {
        return orderRepository.findById(id).get();
    }

    @Override
    public Order saveData(Order data) {
        return orderRepository.save(data);
    }

    @Override
    public ResponseEntity<Order> updateData(Order data, int id) {
        Optional<Order> order = orderRepository.findById(id);

        if (!order.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        data.setId(id);
        orderRepository.save(data);
        return ResponseEntity.ok(data);
    }

    @Override
    public ResponseEntity<Order> updateOrderData(int id, int employee_id) {
        Optional<Order> order = orderRepository.findById(id);
        Optional<Employee> employee = employeeRepository.findById(employee_id);
        Employee employees = employee.get();

        if (!order.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        Order orderNew = order.get();
        orderNew.setId(id);
        orderNew.setLastUpdateDate(LocalDate.now());
        orderNew.setUpdatedBy(employees);

        orderRepository.save(orderNew);
        return ResponseEntity.ok(orderNew);
    }

    @Override
    public void deleteData(int id) {
        Optional<Order> order = orderRepository.findById(id);
        Order orderNew = order.get();

        orderNew.setDeletedDate(LocalDate.now());
        orderRepository.save(orderNew);
    }
}