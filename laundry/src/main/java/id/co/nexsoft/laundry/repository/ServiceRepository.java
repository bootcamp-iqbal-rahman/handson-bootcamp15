package id.co.nexsoft.laundry.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.laundry.model.Services;

public interface ServiceRepository extends CrudRepository<Services, Integer> {
    List<Services> findAll();
    Optional<Services> findById(int id);
}
