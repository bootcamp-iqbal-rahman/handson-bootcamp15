package id.co.nexsoft.laundry.model;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "order_items")
public class OrderItems {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "order_id")
    @JsonBackReference
    private Order order_id;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "item_name")
    private String itemName;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "weight")
    private String weight;

    @NotNull(message = "Field cannot be null")
    @Column(name = "amount")
    private double amount;

    @ManyToOne
    @JoinColumn(name = "service_id")
    private Services service_id;

    @Column(name = "deleted_date")
    private LocalDate deletedDate;

    public OrderItems() {}

    public OrderItems(Order order_id, String itemName, String weight, 
                double amount, Services service_id) {
        this.order_id = order_id;
        this.itemName = itemName;
        this.weight = weight;
        this.amount = amount;
        this.service_id = service_id;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Order getOrder_id() {
        return order_id;
    }

    public void setOrder_id(Order order) {
        this.order_id = order;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getWeight() {
        return weight;
    }

    public void setWeight(String weight) {
        this.weight = weight;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Services getService_id() {
        return service_id;
    }

    public void setService_id(Services service_id) {
        this.service_id = service_id;
    }

    public void setDeletedDate(LocalDate deletedDate) {
        this.deletedDate = deletedDate;
    }

    public LocalDate getDeletedDate() {
        return deletedDate;
    }
}