package id.co.nexsoft.laundry.service;

import org.springframework.http.ResponseEntity;

public interface UpdateService<T> {
    ResponseEntity<T> updateOrderData(int id, int employee_id);
}
