package id.co.nexsoft.laundry.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.laundry.model.Address;

public interface AddressRepository extends CrudRepository<Address, Integer> {
    List<Address> findAll();
    Optional<Address> findById(int id);
}
