package id.co.nexsoft.laundry.model;

import java.time.LocalDate;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.CascadeType;
import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "first_name")
    private String firstName;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "last_name")
    private String lastName;

    @OneToMany(mappedBy = "customer_id", cascade = CascadeType.ALL)
    @JsonManagedReference(value = "customer-address")
    private List<Address> addres_list;
    
    @Pattern(regexp = "^[0-9]+$", message = "Phone number should only contain numbers")
    @Column(name = "phone")
    private long phone;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "email")
    private String email;

    @Pattern(regexp = "\\b(.*\\.png|.*\\.jpg)\\b", message = "Only png and jpg supported")
    @Column(name = "avatar")
    private String avatar;

    @Column(name = "deleted_date")
    private LocalDate deletedDate;

    public Customer() {}

    public Customer(String firstName, String lastName, long phone, String email, String avatar) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.phone = phone;
        this.email = email;
        this.avatar = avatar;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Address> getAddres_list() {
        return addres_list;
    }

    public void setAddres_list(List<Address> addres_list) {
        this.addres_list = addres_list;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }   

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public void setDeletedDate(LocalDate deletedDate) {
        this.deletedDate = deletedDate;
    }

    public LocalDate getDeletedDate() {
        return deletedDate;
    }
}
