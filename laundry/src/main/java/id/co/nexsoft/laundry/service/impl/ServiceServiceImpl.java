package id.co.nexsoft.laundry.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import id.co.nexsoft.laundry.model.Services;
import id.co.nexsoft.laundry.repository.ServiceRepository;
import id.co.nexsoft.laundry.service.DefaultService;

@Service
public class ServiceServiceImpl implements DefaultService<Services> {
    @Autowired
    private ServiceRepository serviceRepository;
    
    @Override
    public List<Services> getAllData() {
        return serviceRepository.findAll();
    }

    @Override
    public Services getDataById(int id) {
        return serviceRepository.findById(id).get();
    }

    @Override
    public Services saveData(Services data) {
        return serviceRepository.save(data);
    }

    @Override
    public ResponseEntity<Services> updateData(Services data, int id) {
        Optional<Services> services = serviceRepository.findById(id);

        if (!services.isPresent()) {
            return ResponseEntity.notFound().build();
        }

        data.setId(id);
        serviceRepository.save(data);
        return ResponseEntity.ok(data);
    }

    @Override
    public void deleteData(int id) {
        Optional<Services> serviceList = serviceRepository.findById(id);
        Services services = serviceList.get();

        services.setDeletedDate(LocalDate.now());
        serviceRepository.save(services);
    }
    
}
