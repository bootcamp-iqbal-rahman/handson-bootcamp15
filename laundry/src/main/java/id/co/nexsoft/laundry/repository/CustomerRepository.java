package id.co.nexsoft.laundry.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.laundry.model.Customer;

public interface CustomerRepository extends CrudRepository<Customer, Integer> {
    List<Customer> findAll();
    Optional<Customer> findById(int id);
}
