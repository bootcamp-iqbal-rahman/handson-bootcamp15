package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.Address;
import id.co.nexsoft.laundry.service.DefaultService;
import jakarta.validation.Valid;

@RestController
@Validated
@RequestMapping(path = "/address")
public class AddressController {
    @Autowired
    private DefaultService<Address> defaultService;

    @GetMapping
    public List<Address> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Address getDataById(@Valid @PathVariable int id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Address saveData(@Valid @RequestBody Address data) {
        return defaultService.saveData(data);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Address> updateData(@Valid @RequestBody Address data, @PathVariable int id) {
        return defaultService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        defaultService.deleteData(id);
    }
}