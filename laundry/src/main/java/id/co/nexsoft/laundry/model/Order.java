package id.co.nexsoft.laundry.model;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "orders")
public class Order {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @OneToMany(mappedBy = "order_id")
    @JsonManagedReference
    private List<OrderItems> order_items;

    @NotNull(message = "Field cannot be null")
    @Column(name = "order_date")
    private LocalDate orderDate;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "status")
    private String status;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "payment_status")
    private String paymentStatus;

    @Column(name = "last_update_date")
    private LocalDate lastUpdateDate;

    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee updatedBy;
    
    @Column(name = "deleted_date")
    private LocalDate deletedDate;

    public Order() {}

    public Order(LocalDate orderDate, String status, String paymentStatus, 
                LocalDate lastUpdateDate, Employee employee) {
        this.orderDate = orderDate;
        this.status = status;
        this.paymentStatus = paymentStatus;
        this.lastUpdateDate = lastUpdateDate;
        this.updatedBy = employee;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDate getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(LocalDate orderDate) {
        this.orderDate = orderDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    public LocalDate getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(LocalDate lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public Employee getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(Employee updatedBy) {
        this.updatedBy = updatedBy;
    }

    public void setDeletedDate(LocalDate deletedDate) {
        this.deletedDate = deletedDate;
    }

    public LocalDate getDeletedDate() {
        return deletedDate;
    }
}