package id.co.nexsoft.laundry.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import id.co.nexsoft.laundry.model.Employee;

public interface EmployeeRepository extends CrudRepository<Employee, Integer> {
    List<Employee> findAll();
    Optional<Employee> findById(int id);
}
