package id.co.nexsoft.laundry.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import id.co.nexsoft.laundry.model.Employee;
import id.co.nexsoft.laundry.service.DefaultService;
import jakarta.validation.Valid;

@Validated
@RestController
@RequestMapping(path = "/employee")
public class EmployeeController {
    @Autowired
    private DefaultService<Employee> defaultService;

    @GetMapping
    public List<Employee> getAllData() {
        return defaultService.getAllData();
    }

    @GetMapping("/{id}")
    public Employee getDataById(@Valid @PathVariable int id) {
        return defaultService.getDataById(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Employee saveData(@Valid @RequestBody Employee data) {
        return defaultService.saveData(data);
    }

    @PutMapping("/{id}")
    public ResponseEntity<Employee> updateData(@Valid @RequestBody Employee data, @PathVariable int id) {
        return defaultService.updateData(data, id);
    }

    @DeleteMapping("/{id}")
    public void deleteData(@PathVariable int id) {
        defaultService.deleteData(id);
    }
}