package id.co.nexsoft.laundry.model;

import java.time.LocalDate;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "service")
public class Services {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "name")
    private String name;

    @NotNull(message = "Field cannot be null")
    @Column(name = "price")
    private double price;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "unit")
    private String unit;

    @NotBlank(message = "Field cannot be blank")
    @NotNull(message = "Field cannot be null")
    @Column(name = "type")
    private String type;

    @Column(name = "deleted_date")
    private LocalDate deletedDate;

    public Services() {}

    public Services(String name, double price, String unit, String type) {
        this.name = name;
        this.price = price;
        this.unit = unit;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public LocalDate getDeletedDate() {
        return deletedDate;
    }

    public void setDeletedDate(LocalDate deletedDate) {
        this.deletedDate = deletedDate;
    }
}
