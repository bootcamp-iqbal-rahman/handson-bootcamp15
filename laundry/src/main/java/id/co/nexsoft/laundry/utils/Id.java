package id.co.nexsoft.laundry.utils;

public class Id {
    private int id;

    private Id() {}
    
    public Id(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
